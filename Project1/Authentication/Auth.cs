﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Runtime.Serialization.Json;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Text;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Configuration;

namespace Project1.Authentication
{
    public class ManualAuth
    {
        public async Task<string> RunAsync(string access_token)
        {
            using (HttpClient client = new HttpClient())
            {
                string client_id = WebConfigurationManager.AppSettings["clientId"];
                string client_secret = WebConfigurationManager.AppSettings["client_secret"];
                string username = WebConfigurationManager.AppSettings["username"];
                string password = WebConfigurationManager.AppSettings["password"];

                var value = new Dictionary<string, string>
                 {
                     {"grant_type","password" },
                     {"client_id",client_id},
                     {"resource","https://acejavier.crm4.dynamics.com"},
                     {"client_secret",client_secret},
                     {"username",username},
                     {"password",password}
                 };
                var content = new FormUrlEncodedContent(value);
                HttpResponseMessage response = await client.PostAsync("https://login.microsoftonline.com/12219ad4-f464-4c08-b0ae-8e278cf55be8/oauth2/token", content);
                string resp = await response.Content.ReadAsStringAsync();
                JObject responseObject = JObject.Parse(resp);
                access_token = responseObject["access_token"].ToString();
                return access_token;
            }
        }
    }
}