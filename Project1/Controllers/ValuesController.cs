﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.Serialization.Json;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Text;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.Crm.Sdk.Samples.HelperCode;
using System.Web.Http;
using RestSharp;
using Project1.Authentication;

namespace CRMWebAPI.Controllers
{
    public class ValuesController : ApiController
    {

        private List<Object> entityUris = new List<Object>();

        public async Task<Object> Get() // GET ALL
        {
            using (HttpClient client = new HttpClient())
            {
                ManualAuth bearertoken = new ManualAuth();
                string authtoken= " ";
                var res = new ManualAuth();
                var tokenparam = await res.RunAsync(authtoken);              
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + tokenparam);
                HttpResponseMessage response = await client.GetAsync("https://acejavier.crm4.dynamics.com/api/data/v9.0/contacts");
                string resp = await response.Content.ReadAsStringAsync();
                JObject result = JObject.Parse(resp);
                return result;
            }
        }

        public async Task<Object> Get(string contactid) // GET WITH ID
        {
          
            using (HttpClient client = new HttpClient())
            {
                ManualAuth bearertoken = new ManualAuth();
                string authtoken = " ";
                var res = new ManualAuth();
                var tokenparam = await res.RunAsync(authtoken);
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + tokenparam);
                HttpResponseMessage response = await client.GetAsync($"https://acejavier.crm4.dynamics.com/api/data/v9.0/contacts({contactid})");
                string resp = await response.Content.ReadAsStringAsync();
                JObject result = JObject.Parse(resp);
                return result;
            }
        } 
        public async Task<object> Patch([FromBody] Object json, [FromUri] Guid contactid) // UPDATE
        {
                ManualAuth bearertoken = new ManualAuth();
                string authtoken = " ";
                var res = new ManualAuth();
                var tokenparam = await res.RunAsync(authtoken);
                var client = new RestClient($"https://acejavier.crm4.dynamics.com/api/data/v9.0/contacts({contactid})");
                var request = new RestRequest(Method.PATCH);
                request.AddHeader("Postman-Token", "5df9e824-1112-4e02-88ee-f4922c3b3e16");
                request.AddHeader("Authorization", "Bearer " + tokenparam);
                request.AddHeader("Content-Type", "application/json");
                request.AddParameter("value", json, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                if (response.IsSuccessful)
                {
                    return new HttpResponseMessage(HttpStatusCode.OK);
                }
                else
                {
                    return response.StatusDescription;
                }           
       }
        public async Task<Object> Post([FromBody] Object json) // CREATE
        {            
                ManualAuth bearertoken = new ManualAuth();
                string authtoken = " ";
                var res = new ManualAuth();
                var tokenparam = await res.RunAsync(authtoken);           
                var client = new RestClient("https://acejavier.crm4.dynamics.com/api/data/v9.0/contacts");
                var request = new RestRequest(Method.POST);
                request.AddHeader("Postman-Token", "5df9e824-1112-4e02-88ee-f4922c3b3e16");
                request.AddHeader("Authorization", "Bearer " + tokenparam);
                request.AddHeader("Content-Type", "application/json");
                request.AddParameter("value", json, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                if (response.IsSuccessful)
                {
                 return new HttpResponseMessage(HttpStatusCode.OK);
                }
                else
                {
                    return response.StatusDescription;
                }
        }
        public async Task<Object> Delete([FromUri] string contactid) // DELETE
        {
                ManualAuth bearertoken = new ManualAuth();
                string authtoken = " ";
                var res = new ManualAuth();
                var tokenparam = await res.RunAsync(authtoken);
                var client = new RestClient($"https://acejavier.crm4.dynamics.com/api/data/v9.0/contacts({contactid})");
                var request = new RestRequest(Method.DELETE);
                request.AddHeader("Postman-Token", "10725242-addc-4268-a27b-33ce5c6b6fbe");
                request.AddHeader("Cache-Control", "no-cache");
                request.AddHeader("Authorization", "Bearer " + tokenparam);
                request.AddHeader("Content-Type", "application/json");
                //  request.AddParameter("value",contactid, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                if (response.IsSuccessful)
                {
                    return new HttpResponseMessage(HttpStatusCode.OK);
                }
                else
                {
                    return response.StatusDescription;
                }           
        }

    }
}